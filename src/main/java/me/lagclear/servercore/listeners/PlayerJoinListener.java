package me.lagclear.servercore.listeners;

import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

public class PlayerJoinListener implements Listener {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public PlayerJoinListener(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @EventHandler
    private void onJoin(PlayerJoinEvent event) {

        Player player = event.getPlayer();

        if (configUtils.getConfig().getBoolean("options.motd-on-join")) {
            for (String s : configUtils.getList("motd")) {
                if (s.equalsIgnoreCase("\"\"")) {
                    player.sendMessage("");
                    continue;
                }
                player.sendMessage(languageUtils.color(s));
            }
        }

        for (String s : configUtils.getConfig().getStringList("join-actions")) {
            if (s.contains("chat:")) {
                player.chat(languageUtils.color(s.replace("chat:", "")));
                continue;
            }

            player.performCommand(s);
        }
    }
}
