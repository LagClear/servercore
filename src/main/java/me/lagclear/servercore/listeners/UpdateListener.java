package me.lagclear.servercore.listeners;

import me.lagclear.servercore.utils.ConfigUtils;
import org.bukkit.Bukkit;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.Plugin;

import javax.net.ssl.HttpsURLConnection;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class UpdateListener implements Listener {

    private Plugin plugin;
    private ConfigUtils configUtils;

    public UpdateListener(Plugin plugin, ConfigUtils configUtils) {
        this.plugin = plugin;
        this.configUtils = configUtils;
    }

    private final String apiUrl = "https://api.spigotmc.org/legacy/update.php?resource=64015/";
    private final String resourceURL = "https://www.spigotmc.org/resources/servercore.64015/";

    @EventHandler
    public void onPlayerJoin(PlayerJoinEvent event) {
        if (!configUtils.getConfig().getBoolean("options.updater-enabled")) {
            return;
        }

        if (event.getPlayer().hasPermission("servercore.admin")) {
            return;
        }

        Bukkit.getScheduler().runTaskAsynchronously(plugin, () -> {
            try {
                HttpsURLConnection connection = (HttpsURLConnection) new URL(apiUrl).openConnection();
                String version = new BufferedReader(new InputStreamReader(connection.getInputStream())).readLine();

                if (!plugin.getDescription().getVersion().equalsIgnoreCase(version)) {
                    event.getPlayer().sendMessage("Your plugin Is out of Date!");
                    event.getPlayer().sendMessage("Click Here To Update: " + resourceURL);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
