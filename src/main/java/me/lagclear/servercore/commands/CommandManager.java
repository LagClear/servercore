package me.lagclear.servercore.commands;

import me.lagclear.servercore.commands.subcommands.AddCommand;
import me.lagclear.servercore.commands.subcommands.ReloadCommand;
import me.lagclear.servercore.commands.subcommands.RemoveCommand;
import me.lagclear.servercore.commands.subcommands.SetCommand;
import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

import java.util.HashSet;
import java.util.Set;

public class CommandManager implements CommandExecutor {

    private Set<CommandBase> commands;

    private LanguageUtils languageUtils;

    public CommandManager(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.languageUtils = languageUtils;

        commands = new HashSet<>();

        commands.add(new AddCommand(configUtils, languageUtils));
        commands.add(new ReloadCommand(configUtils, languageUtils));
        commands.add(new RemoveCommand(configUtils, languageUtils));
        commands.add(new SetCommand(configUtils, languageUtils));

    }

    @Override
    public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {

        if (args.length == 0) {

            if (sender.hasPermission("servercore.admin")) {
                for (String s : languageUtils.getList("staff-help")) {
                    sender.sendMessage(languageUtils.color(s));
                }
                return true;
            }
            sender.sendMessage(languageUtils.getMessage("no-permission", true));
            return true;

        }

        CommandBase command = getCommand(args[0]);

        if (command == null) {
            sender.sendMessage(languageUtils.getMessage("invalid-sub-cmd", true));
            return true;
        }

        command.onCommand(sender, args);

        return true;
    }

    private CommandBase getCommand(String command) {
        for (CommandBase commandBase : commands) {
            if (commandBase.getCommand().equalsIgnoreCase(command)) {
                return commandBase;
            }
        }
        return null;
    }
}

