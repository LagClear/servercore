package me.lagclear.servercore.commands;

import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class StoreCommand implements CommandExecutor {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public StoreCommand(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!configUtils.getConfig().getBoolean("options.store-enabled")) {
            sender.sendMessage(languageUtils.getMessage("command-disabled", true));
            return true;
        }

        for (String s : configUtils.getConfig().getStringList("store")) {
            if (s.equalsIgnoreCase("\"\"")) {
                sender.sendMessage("");
                continue;
            }
            sender.sendMessage(languageUtils.color(s));
        }
        return true;
    }
}
