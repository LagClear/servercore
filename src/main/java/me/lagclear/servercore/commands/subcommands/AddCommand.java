package me.lagclear.servercore.commands.subcommands;

import me.lagclear.servercore.commands.CommandBase;
import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.CommandSender;

import java.util.List;

public class AddCommand extends CommandBase {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public AddCommand(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (!sender.hasPermission("servercore.admin")) {
            sender.sendMessage(languageUtils.getMessage("no-permission", true));
            return;
        }

        if (args.length == 1) {
            sender.sendMessage(languageUtils.getMessage("add-example", true));
            return;
        }

        if (args.length == 2) {
            sender.sendMessage(languageUtils.getMessage("specify-message", true));
            return;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 2; i < args.length; i++) {
            sb.append(args[i]).append(" ");
        }

        List<String> list = configUtils.getList(args[1]);

        if (list == null) {
            sender.sendMessage(languageUtils.getMessage("invalid-type", true));
            return;
        }

        list.add(sb.toString().trim());
        sender.sendMessage(languageUtils.getMessage("confirm-add", true)
                .replace("{cmd}", args[1].toLowerCase()));
        configUtils.saveConfig();
    }

    @Override
    public String getCommand() {
        return "add";
    }
}
