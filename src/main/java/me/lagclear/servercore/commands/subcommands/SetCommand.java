package me.lagclear.servercore.commands.subcommands;

import me.lagclear.servercore.commands.CommandBase;
import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.CommandSender;

import java.util.List;

public class SetCommand extends CommandBase {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public SetCommand(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (!sender.hasPermission("servercore.admin")) {
            sender.sendMessage(languageUtils.getMessage("no-permission", true));
            return;
        }

        if (args.length == 1) {
            sender.sendMessage(languageUtils.getMessage("set-example", true));
            return;
        }

        if (args.length == 2) {
            sender.sendMessage(languageUtils.getMessage("specify-message", true));
            return;
        }

        int index;

        try {
            index = Integer.parseInt(args[2]);
        } catch (NumberFormatException e) {
            sender.sendMessage(languageUtils.getMessage("invalid-number", true));
            return;
        }

        StringBuilder sb = new StringBuilder();

        for (int i = 3; i < args.length; i++) {
            sb.append(args[i]).append(" ");
        }

        List<String> list = configUtils.getList(args[1]);

        if (list == null) {
            sender.sendMessage(languageUtils.getMessage("invalid-type", true));
            return;
        }

        try {
            list.set(index, sb.toString());
        } catch (IndexOutOfBoundsException e) {
            sender.sendMessage(languageUtils.color(languageUtils.getMessage("no-line-found", true)));
            return;
        }

        sender.sendMessage(languageUtils.getMessage("confirm-set", true)
                .replace("{line}", Integer.toString(index))
                .replace("{cmd}", args[1].toLowerCase()));
        configUtils.saveConfig();
    }

    @Override
    public String getCommand() {
        return "set";
    }
}
