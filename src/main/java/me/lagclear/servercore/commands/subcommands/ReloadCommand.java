package me.lagclear.servercore.commands.subcommands;

import me.lagclear.servercore.commands.CommandBase;
import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.CommandSender;

public class ReloadCommand extends CommandBase {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public ReloadCommand(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @Override
    public void onCommand(CommandSender sender, String[] args) {
        if (!sender.hasPermission("servercore.admin")) {
            sender.sendMessage(languageUtils.getMessage("no-permission", true));
            return;
        }

        if (args.length == 1) {
            configUtils.loadConfig();
            languageUtils.loadLang();
            sender.sendMessage(languageUtils.getMessage("files-reloaded", true));
            return;
        }

        if (args.length == 2) {
            if (args[1].equalsIgnoreCase("config")) {
                configUtils.loadConfig();
                sender.sendMessage(languageUtils.getMessage("config-reloaded", true));
                return;
            }

            if (args[1].equalsIgnoreCase("lang")) {
                languageUtils.loadLang();
                sender.sendMessage(languageUtils.getMessage("lang-reloaded", true));
                return;
            }

            sender.sendMessage(languageUtils.getMessage("invalid-file-name", true));
        }
    }

    @Override
    public String getCommand() {
        return "reload";
    }
}
