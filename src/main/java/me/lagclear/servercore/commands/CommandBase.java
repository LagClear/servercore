package me.lagclear.servercore.commands;

import org.bukkit.command.CommandSender;

public abstract class CommandBase {

    public abstract void onCommand(CommandSender sender, String[] args);

    public abstract String getCommand();

}

