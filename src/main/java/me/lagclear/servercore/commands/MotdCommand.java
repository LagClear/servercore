package me.lagclear.servercore.commands;

import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;

public class MotdCommand implements CommandExecutor {

    private ConfigUtils configUtils;
    private LanguageUtils languageUtils;

    public MotdCommand(ConfigUtils configUtils, LanguageUtils languageUtils) {
        this.configUtils = configUtils;
        this.languageUtils = languageUtils;
    }

    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (!configUtils.getConfig().getBoolean("options.motd-enabled")) {
            sender.sendMessage(languageUtils.getMessage("command-disabled", true));
            return true;
        }

        for (String s : configUtils.getConfig().getStringList("motd")) {
            if (s.equalsIgnoreCase("\"\"")) {
                sender.sendMessage("");
                continue;
            }
            sender.sendMessage(languageUtils.color(s));
        }
        return true;
    }
}
