package me.lagclear.servercore;

import me.lagclear.servercore.commands.*;
import me.lagclear.servercore.listeners.PlayerJoinListener;
import me.lagclear.servercore.listeners.UpdateListener;
import me.lagclear.servercore.utils.ConfigUtils;
import me.lagclear.servercore.utils.LanguageUtils;
import org.bukkit.plugin.java.JavaPlugin;

public final class ServerCore extends JavaPlugin {

    @Override
    public void onEnable() {
        // Create Instances
        LanguageUtils languageUtils = new LanguageUtils(this);
        ConfigUtils configUtils = new ConfigUtils(this);

        // Load Files
        configUtils.loadConfig();
        languageUtils.loadLang();

        // Register Commands
        getCommand("servercore").setExecutor(new CommandManager(configUtils, languageUtils));
        getCommand("discord").setExecutor(new DiscordCommand(configUtils, languageUtils));
        getCommand("forum").setExecutor(new ForumCommand(configUtils, languageUtils));
        getCommand("info").setExecutor(new InfoCommand(configUtils, languageUtils));
        getCommand("motd").setExecutor(new MotdCommand(configUtils, languageUtils));
        getCommand("store").setExecutor(new StoreCommand(configUtils, languageUtils));
        getCommand("website").setExecutor(new WebsiteCommand(configUtils, languageUtils));

        // Register Listeners
        getServer().getPluginManager().registerEvents(new PlayerJoinListener(configUtils, languageUtils), this);
        getServer().getPluginManager().registerEvents(new UpdateListener(this, configUtils), this);
    }
}
